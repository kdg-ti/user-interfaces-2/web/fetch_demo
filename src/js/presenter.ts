import * as storeClient from './restclient';
import PersonInterface from './Person.interface';

const idInput = document.getElementById("id") as HTMLInputElement;
const feedback = document.getElementById("feedback");
const postButton = document.getElementById("post");
const deleteButton = document.getElementById("delete");
const getButton = document.getElementById("get");
const getsButton = document.getElementById("gets");

function showMessage(message: string) {
    if (feedback) {
        feedback.innerHTML = message;
    }
}

function showError(message: string, error: unknown) {
    showMessage(`Error ${message}: ${error}`);
}

async function showDeletePerson(id: string) {
    console.log('Trying DELETE!');
    try {
        await storeClient.deletePerson(id);
        showMessage(`<strong>DELETE</strong>d person ${id}`);
    } catch (e) {
        showError("DELETING", e);
    }
}

async function showPerson(id: string) {
    console.log('Trying GET!');
    try {
        const person = await storeClient.getPerson(id);
        showMessage(`<strong>GET</strong> succeeded<pre><code>${JSON.stringify(person, null, 4)}</code></pre>`);
    } catch (e) {
        showError("GETTING " + id, e);
    }
}

async function showPersons() {
    console.log('Trying GET all persons!');
    try {
        const persons = await storeClient.getPersons();
        // Use a reduce operation to create a string with a html <table>
        const table = persons
            .reduce(
                (acc: string, person: PersonInterface) => {
                    return acc + `<tr><td>${person.id}</td><td>${person.firstName}</td><td>${person.lastName}</td></tr>`;
                },
                "<h4>GET all succeeded</h4><table>"
            );

        showMessage(table + "</table>");

    } catch (e) {
        showError("GETTING all", e);
    }
}

export default function addListeners() {
    if (postButton) {
        postButton.addEventListener("click", async (event) => {

            console.log('Trying POST...');
            try {
                const responseData = await storeClient.postPerson({
                    firstName: "Jos",
                    lastName: "Bosmans",
                    dateOfBirth: "1963-04-26",
                    gender: "M",
                    married: true,
                    image: "https://via.placeholder.com/200x80.png/123/FFF?text=Jos",
                    yearsService: 3
                });
                showMessage('<h4>POST gelukt</h4>' + JSON.stringify(responseData));
            } catch (e) {
                showError('Error bij POST:', e);
            }
        });
    }

    if (deleteButton) {
        deleteButton.addEventListener("click", () => showDeletePerson(idInput.value));
    }

    if (getButton) {
        getButton.addEventListener("click", () => showPerson(idInput.value));
    }

    if (getsButton) {
        getsButton.addEventListener("click", showPersons);
    }
}
