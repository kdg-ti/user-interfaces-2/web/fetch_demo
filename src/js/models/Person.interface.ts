export default interface PersonInterface {
    id?: string // optional to allow for empty id when posting
    firstName: string,
    lastName: string,
    dateOfBirth: string,
    gender: string,
    married?: boolean,
    image?: string,
    yearsService?: number
}