import PersonInterface from './Person.interface';

const PERSONS_URL = 'http://localhost:3000/persons';

export async function postPerson(person: PersonInterface) {
  try {
    const response = await fetch(PERSONS_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(person)
    });

    if (!response.ok) {
      throw new Error(`Unable to POST person ${person?.id}: ${response.status} ${response.statusText}`);
    }

    return response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function deletePerson(id: string) {
  try {
    const response = await fetch(`${PERSONS_URL}/${id}`, { method: "DELETE" });

    if (!response.ok) {
      throw new Error(`Unable to DELETE person ${id}: ${response.status} ${response.statusText}`);
    }

    return response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function getPerson(id: string) {
  try {
    const response = await fetch(`${PERSONS_URL}/${id}`);

    if (!response.ok) {
      throw new Error(`Unable to GET person ${id}: ${response.status} ${response.statusText}`);
    }

    return response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function getPersons() {
  try {
    const response = await fetch(PERSONS_URL);

    if (!response.ok) {
      throw new Error(`Unable to GET all persons: ${response.status} ${response.statusText}`);
    }

    return response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
}
