# About

## Usage

```
$ npm install
```

- Zorg dat je een json-server hebt draaien met daarin de 'persons' json demodata

```
$ npm run rest
```

- in een andere terminal start je nu de toepassing:

```
$ npm start
```

- Test nu de `POST`, `DELETE` en `GET`
- Inspecteer de code, check zeker ook de HTTP requests (bv in de developer tools van je browser)
- Test wat er gebeurt bij fouten: breng de json-server eens down en test, gebruik eens een foute URL en test, ...
